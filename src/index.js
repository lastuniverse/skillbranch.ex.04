import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());



String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}




app.get('/', (req, res) => {
  console.log(req.query);

  if( !req.query.fullname ){
    res.send('Invalid fullname');
    return;
  }

//  const re = /^\s*((((\w)\w+)\s+)?((\w)\w+)\s+)?((\w)\w+)\s*$/i;

  //const w = /\u0000-\u007F\u0080-\u00FF\u0100-\u017F\u0180-\u024F\u0370-\u03FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0600-\u06FF\u0700-\u074F\u0750-\u077F\u0780-\u07BF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u0D80-\u0DFF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u1380-\u139F\u13A0-\u13FF\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u1980-\u19DF\u19E0-\u19FF\u1A00-\u1A1F\u1B00-\u1B7F\u1E00-\u1EFF\u1F00-\u1FFF/;
  const w = '[\\\'A-Za-z\\u0080-\\u00FF\\u0400-\\u04FF]';
  const re = new RegExp('^\\s*(((('+w+')'+w+'+?)\\s+)?(('+w+')'+w+'+?)\\s+)?('+w+'+?)\\s*$','');

  //const re = new RegExp("\^((((\\w)\\w+)\\s+)?((\\w)\\w+)\\s+)?((\\w)\\w+)\$",'i');
  //const re = /^((((\w)\w+)\s+)?((\w)\w+)\s+)?((\w)\w+)$/i;




  const fullname = re.exec(req.query.fullname);
  console.log(fullname);

  let ret = '';
  if( !fullname || !fullname[7] ){
    ret = 'Invalid fullname';
  }else if( fullname[3] ){
    ret = fullname[7].toLowerCase().capitalizeFirstLetter()+' '+fullname[4].capitalizeFirstLetter()+'. '+fullname[6].capitalizeFirstLetter()+'.';
  }else if( fullname[5] ){
    ret = fullname[7].toLowerCase().capitalizeFirstLetter()+' '+fullname[6].capitalizeFirstLetter()+'.';
  }else if( fullname[7] ){
    ret = fullname[7].toLowerCase().capitalizeFirstLetter();
  }
  console.log(ret);
  res.send(ret);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
